import { DevicesAPI } from './devices';
import { ProfilesAPI } from './profiles';

export let Devices = new DevicesAPI();
export let Profiles = new ProfilesAPI();
