import request from 'superagent';
import nconf from 'nconf';

nconf.env();

export class ProfilesAPI {
    constructor() {
    }

    get(id, callback) {
        request
            .get(`${nconf.get('PROFILES_API')}/profiles/${id}`)
            .end((err, res) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, res.body);
                }
            });
    }
}
