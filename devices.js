import request from 'superagent';
import nconf from 'nconf';

nconf.env();

export class DevicesAPI {
    constructor() {
    }

    get(uid, callback) {
        request
            .get(`${nconf.get('DEVICES_API')}/devices/${uid}`)
            .end((err, res) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, res.body);
                }
            });
    }

    create(uid, callback) {
        request
            .post(`${nconf.get('DEVICES_API')}/devices`)
            .send({ uid: uid })
            .end((err, res) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, res.body);
                }
            });
    }
}
